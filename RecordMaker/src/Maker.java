
public class Maker {
	
	String makerNum;	//사원번호
	int makerScore;		//사원점수
	int makerNo;		//사원시퀀스
	
	public Maker(String makerNum, int makerScore, int makerNo) {
		this.makerNum = makerNum;
		this.makerScore = makerScore;
		this.makerNo = makerNo;
	}
	
	
	public int getMakerNo() {
		return makerNo;
	}
	public void setMakerNo(int makerNo) {
		this.makerNo = makerNo;
	}
	public String getMakerNum() {
		return makerNum;
	}
	public void setMakerNum(String makerNum) {
		this.makerNum = makerNum;
	}
	public int getMakerScore() {
		return makerScore;
	}
	public void setMakerScore(int makerScore) {
		this.makerScore = makerScore;
	}
	
}
