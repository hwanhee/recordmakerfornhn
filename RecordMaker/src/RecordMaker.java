import java.util.ArrayList;

public class RecordMaker {

	static ArrayList<Maker> makerList;

	public static void main(String[] args) {

		makerList = new ArrayList<>();	//사원 배열
		makeMaker(Integer.parseInt(args[0]));	//사원을 매개변수만큼 생성하는 함수
		
		//사원 출력!
		for(Maker maker : makerList) 
			System.out.println(maker.makerNum+"\t"+maker.makerScore);
	}

	static void makeMaker(int mCount) {

		for (int i = 0; i < mCount; i++) {
			
			int makerNo = (int) (Math.random() * 100000);	//사원번호를 만들기 위한 Sequence 0~99999 램덤생성

			int flag = 0;	//중복 값 확인을 위한 Flag
			
			for (Maker maker : makerList) {		//Sequence가 중복됐는지 루프돌며 확인
				if (maker.makerNo == makerNo)
					flag = 1;
			}

			
			//중복되지 않은 경우
			if (flag < 1) {
				
				//사원번호를 NT00001 형식으로 생성
				String makerNum = "NT";
				if ((makerNo + "").length() < 5) {
					String tmp = "0" + makerNo;
					while (tmp.length() < 5) {
						tmp = "0" + tmp;
					}
					makerNum = makerNum + tmp;

				} else
					makerNum = makerNum + makerNo;

				//사원점수 0-99 램덤생성
				int makerScore = (int) (Math.random() * 100);

				//사원을 출력배열에 추가
				makerList.add(new Maker(makerNum, makerScore, makerNo));
			}
			
			//중복된 경우 COUNT에 맞게 사원을 출력하기 위해 i-1
			else i-=1;
		}

	}

}
